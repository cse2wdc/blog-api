const express = require('express');

const router = express.Router();

// Index
router.get('/', (req, res) => {
  res.json({ todo: 'List posts' });
});

// TODO: Create

// Show
// Note that the path contains a variable (the :postId part). This will be
// made available as a property of the req.params object.
router.get('/:postId', (req, res) => {
  res.json({ todo: 'Show post with ID' + req.params.postId });
});

// TODO: Destroy, update

module.exports = router;
